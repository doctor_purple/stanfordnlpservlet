

import java.io.IOException;
import java.text.ParseException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import edu.stanford.nlp.io.EncodingPrintWriter.out;
import fr.amadeus.exceptions.SemanticException;
import fr.amadeus.nlp.NLPHandler;
import fr.amadeus.nlp.TravelSemantic;

/**
 * Servlet implementation class MainServlet
 */
@WebServlet("/MainServlet")
public class MainServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
    private NLPHandler nlpHandler = new NLPHandler();
    
    
    //keys for post request
  	private static final String TAG_ID = "id";
  	private static final String TAG_MESSAGE = "message";
  	private static final String VALUE_TAG_ID = "01";
  	
  	//keys of the json and the hashmap
  	private static final String TAG_ORIGIN = "origin";
  	private static final String TAG_DESTINATION = "destination";
  	private static final String	TAG_TEXT = "text";
  	private static final String TAG_LATITUDE = "latitude";
  	private static final String	TAG_LONGITUDE = "longitude";
  	private static final String TAG_DEPARTURE = "departure";
  	private static final String TAG_TICKETS = "tickets";
  	private static final String TAG_CLASS = "ticket_class";
     
    //JSONOBejcts and arrays
    private JSONObject jsonObject;
    private JSONObject text;
    private JSONObject latitude;
    private JSONObject longitude;
    private JSONObject CRBCode;
    private JSONArray origin;
    private JSONArray destination;
    
    
 
    public MainServlet() throws IOException, ClassNotFoundException {
        super();
    }
    
    
    public void doGet(HttpServletRequest request,
            HttpServletResponse response)
        throws ServletException, IOException {
    	 
    	//take the parameter values from the request
    	String id = request.getParameter(TAG_ID);
    	String message = request.getParameter(TAG_MESSAGE);
    	float lat = 0, lng = 0;
    	if (request.getParameter(TAG_LATITUDE) != null) {
    		lat = Float.valueOf(request.getParameter(TAG_LATITUDE));
    	}	
    	if (request.getParameter(TAG_LONGITUDE) != null) {
    		lng  = Float.valueOf(request.getParameter(TAG_LONGITUDE));
    	}
    	
    
		//NLP process message returned from the HTTP post request
    	if (id != null && id.equals(VALUE_TAG_ID)) {
    		if (message != null) {
    			nlpHandler.getTags(message);
            	nlpHandler.buildDataStructure();
    		}

        	//semantic analysis
        	TravelSemantic ts = new TravelSemantic(nlpHandler.getText(), nlpHandler.getEntities(), lat, lng);
    	    
			try {
				ts.semanticAnalyzer();
			} catch (SemanticException e) {
				System.out.println(e.getMessage());
			} //get results string from semantic analyzer 
        	

        	
			//creation of Json Object
			jsonObject = new JSONObject(); //higher hierarchical object
			text = new JSONObject(); 
			latitude = new JSONObject();
			longitude = new JSONObject();
			CRBCode = new JSONObject();
			origin = new JSONArray();
			
			if (ts.getOrigin() != null) {
				text.put(TAG_TEXT, ts.getOrigin().get(0)); //origin text
				latitude.put(TAG_LATITUDE, ts.getOrigin().get(1)); //origin latitude;
				longitude.put(TAG_LONGITUDE, ts.getOrigin().get(2)); //origin longitude
				CRBCode.put(TAG_ID, ts.getOrigin().get(3)); //id CRB code
				origin.put(text);
				origin.put(latitude);
				origin.put(longitude);
				origin.put(CRBCode);
				jsonObject.put(TAG_ORIGIN, origin);
			}
			else { 
				//if the origin is null, I should get the closest city respesct to the coordinates
				
				
				
			}
			
			text = new JSONObject(); 
			latitude = new JSONObject();
			longitude = new JSONObject();
			CRBCode = new JSONObject();
			destination = new JSONArray();
			if (ts.getDestination() != null) {
				text.put(TAG_TEXT, ts.getDestination().get(0)); //destination text
				latitude.put(TAG_LATITUDE, ts.getDestination().get(1)); //destination latitude;
				longitude.put(TAG_LONGITUDE, ts.getDestination().get(2)); //destination longitude
				CRBCode.put(TAG_ID, ts.getDestination().get(3)); //id CRB code
				destination.put(text);
				destination.put(latitude);
				destination.put(longitude);
				destination.put(CRBCode);
				jsonObject.put(TAG_DESTINATION, destination);
			}
			
			jsonObject.put(TAG_DEPARTURE, ts.getDate());
			jsonObject.put(TAG_TICKETS, ts.getTicket());
			jsonObject.put(TAG_CLASS, ts.getTicketClass());
			
			//debugging JSON
			System.out.println(jsonObject.toString());
			
			
        	//appends in the response the json object
			response.setContentType("application/json");
        	response.getWriter().write(jsonObject.toString());
   	
    	}
    	else 
    		out.print("not the right device id!");

    }
    
    
    
    // Redirect POST request to GET request.
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
                throws IOException, ServletException {
       doGet(request, response);
    }
}