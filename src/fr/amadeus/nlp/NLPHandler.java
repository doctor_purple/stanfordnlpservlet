package fr.amadeus.nlp;



import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import fr.amadeus.helpers.SQLiteHelper;
import fr.amadeus.models.Sentence;
import fr.amadeus.models.Text;
import fr.amadeus.models.Word;


/*
 * Able to instantiate the StanfordCoreNLP.
 * It has methods that for a group of sentence return the different tag annotators. 
 * There is the method that build the data structure used by the semantic analyzers
 */
public class NLPHandler {
	StanfordCoreNLP pipeline;
	Annotation document;
	List<CoreMap> sentences;
	Word myWord;
	Sentence mySentence;
	Text myText;
	HashMap <String, List<Word>> entities;
	
	public NLPHandler() throws IOException{
		
		//TODO: it starts with the NLPHandler
		//loads the stations in the annotators
		//SQLiteHelper.createStationsAnnotators();

		// creates a StanfordCoreNLP object, with POS tagging, lemmatization, NER, parsing, and coreference resolution 
	    Properties props = new Properties();
	    
	    //annotators of ner can be extended, in this case regexner extends ner entities with the name of the stations, and classes
	    props.put("annotators", "tokenize, ssplit, pos, lemma, ner, regexner");
	    props.put("ner.ignorecase", "true");
	    props.put("regexner.mapping", "fr/amadeus/regexner/stations_annotator");
	    props.put("regexner.ignorecase", "true");
	  	    
	    //add annotators to the stanford engine
	    pipeline = new StanfordCoreNLP(props);
    
	}
	
		
	/*
	 * Returns the List of tags for a set of words
	 * @param sentences
	 */
	public List<CoreMap> getTags(String text) {
		myText = new Text();
		mySentence = new Sentence();
		entities = new HashMap <String, List<Word>>();
		
		// create an empty Annotation just with the given text
		document = new Annotation(text);
			    
		// run all Annotators on this text
		pipeline.annotate(document);
				 
		// these are all the sentences in this document
		// a CoreMap is essentially a Map that uses class objects as keys and has values with custom types
		sentences = document.get(SentencesAnnotation.class);
				
		return sentences;
	}
	
	
	
	
	/*
	 * Builds the data structure
	 * 
	 */
	public void buildDataStructure () {
		
		
		int j = 0; //number of the sentence
		for (CoreMap sentence: sentences) {
		      // traversing the words in the current sentence
		      // a CoreLabel is a CoreMap with additional token-specific methods
			
			int i = 0; //position in the sentence j
			for (CoreLabel token: sentence.get(TokensAnnotation.class)) {
				
		    	  	
				
				String word  = token.originalText();  //the text of the token
				String pos = token.tag();  //POS tag of the token
				String ner = token.ner(); //NER label of the token
				String lemma = token.lemma();  //lemma label of the token	
				int position = i;  //position in the sentence
			       
			    //build the structure of text, containing words and sentences
			    myWord= new Word (position, word, pos, lemma, ner);
			        
			        
		        //DEBUGGING
			    //System.out.println(myWord.toString());
			        

		       
		        mySentence.pushWord(myWord);  //add the word to the words list     
			        

		        //build the entities data structure
		        if (!entities.containsKey(ner)) {
		        	entities.put(ner, new ArrayList<Word>());	
		        }
		         	
		        entities.get(ner).add(myWord);
			  
		        i++; // increment the position of the word       
			}       
		
		myText.pushSentence(mySentence);     
		j++;
		}     
	}	      

	

	
	//----------------------------------------------------GETTERS--------------------------------------------------------------------------
	public Text getText() {
		return myText;	
	}
	
	public HashMap<String, List<Word>> getEntities() {
		return entities;
	}
	
}
