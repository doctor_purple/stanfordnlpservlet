package fr.amadeus.nlp;



import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import fr.amadeus.exceptions.SemanticException;
import fr.amadeus.helpers.DateHelper;
import fr.amadeus.models.Text;
import fr.amadeus.models.Word;
import fr.amadeus.semantics.ClassAnalyzer;
import fr.amadeus.semantics.DateAnalyzer;
import fr.amadeus.semantics.LocationAnalyzer;
import fr.amadeus.semantics.TicketAnalyzer;




public class TravelSemantic {

	//Different Analyzer
	private LocationAnalyzer locationAnalyzer;
	private DateAnalyzer dateAnalyzer;
	private TicketAnalyzer ticketAnalyzer;
	private ClassAnalyzer classAnalyzer;
	
	private String date;
	private List<String> origin, destination;
	private Date d;
	private int number_tickets = 1, class_ticket = 2;
	

	

	public TravelSemantic (Text text, HashMap<String, List<Word>> entities, float latitude, float longitude) {
		
		
		//split the semantic analysis in different areas of search
		locationAnalyzer = new LocationAnalyzer(text, entities, latitude, longitude); //specific semantic parser for locations
		dateAnalyzer = new DateAnalyzer(text, entities); //specific semantic parser for date
		ticketAnalyzer = new TicketAnalyzer(text, entities); //specific semantic parser for the number of tickets
		classAnalyzer = new ClassAnalyzer(text, entities); //specific semantic parser for the class ticket
		
		origin = new ArrayList<String>();
		destination = new ArrayList<String>();
	
	}
	
	
	
	public void semanticAnalyzer () throws SemanticException {
		
		//ticket
		number_tickets = ticketAnalyzer.ticketAnalysis();
				
				
		//class
		class_ticket = classAnalyzer.classAnalysis();
		
		
		//origin, destination
		HashMap<String, List<String>> locations = locationAnalyzer.locationAnalysis();
		destination = locations.get("destination");
		origin = locations.get("origin");
		//date 
		d = dateAnalyzer.dateAnalysis();
		if (d != null) date = DateHelper.toString(d);
		else {
			//when the date is missing, the current date is sent
			date = DateHelper.currentDate();
		}
		
//		if (origin == null)
//			throw new SemanticException("origin missing!");
//		if (destination == null)
//			throw new SemanticException("destination missing!");
//		if (d == null)
//			throw new SemanticException("date missing!");
	}
	
	
	//------------------------------------------------------------------------------------------
	//----------------------------------- 	GETTERS  -------------------------------------------
	//------------------------------------------------------------------------------------------
	public List<String> getOrigin() {
		return origin;
	}
	
	public List<String> getDestination() {
		return destination;
	}
	
	public String getDate() {
		return date;
	}
	
	public int getTicket() {
		return number_tickets;
	}
	
	public int getTicketClass() {
		return class_ticket;
	}

		
	
}
