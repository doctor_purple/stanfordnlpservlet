package fr.amadeus.semantics;

import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ie.NumberNormalizer;
import fr.amadeus.helpers.DateHelper;
import fr.amadeus.helpers.GeneralHelper;
import fr.amadeus.models.Sentence;
import fr.amadeus.models.Text;
import fr.amadeus.models.Word;
import fr.amadeus.tagsets.NERTagset;

public class DateAnalyzer {

	// access to data structure
	Text text;
	HashMap<String, List<Word>> entities;
	List<Word> date_elements;

	// list of words that form week end
	static List<String> week_end = new ArrayList<>();
	static List<String> after_tomorrow = new ArrayList<>();

	// pattern regex for the date
	static Pattern pattern_exact_date;
	static Matcher matcher_exact_date;

	static Pattern pattern_error_date;
	static Matcher matcher_error_date;
	
	String matched_date = null;
	Word matched_month = null;

	// name of month and days of the week
	static String[] month_names;
	static String[] weekdays;

	// Utilities
	Date d = null;
	StringBuffer bufferString;
	int i;

	

	
	public DateAnalyzer(Text text, HashMap<String, List<Word>> entities) {
		this.text = text;
		this.entities = entities;
		date_elements = this.entities.get(NERTagset.DATE.name()); //data structure for the date ner label
																	

		week_end.add("week");
		week_end.add("end");

		after_tomorrow.add("after");
		after_tomorrow.add("tomorrow");

		pattern_exact_date = Pattern.compile("([1-9]|[1-2][0-9]||3[0-1])(st|nd|rd|th)"); //patter for the date "16th of July"s																	
		pattern_error_date = Pattern.compile("(?:^.* )([1-9]|[1-2][0-9]||3[0-1])(?: .*$)"); //pattern for the date without suffix

		//static list of days of the week, and months
		weekdays = new DateFormatSymbols().getWeekdays(); 													
		month_names = new DateFormatSymbols().getMonths(); 
															
		//String buffer utility
		bufferString = new StringBuffer();

	}

	// --------------------------------------------------------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------DATE ANALYSIS------------------------------------------------------------
	// --------------------------------------------------------------------------------------------------------------------------------------------------

	public Date dateAnalysis() {

		if (date_elements != null) {
			int date_occurrencies = entities.get(NERTagset.DATE.name()).size();

			// if the words labeled as DATE are more than one
			if (date_occurrencies >= 1) {
				
				//it contains after tomorrow, tomorrow?
				if (containsWords(date_elements, after_tomorrow))  
					d = DateHelper.setAfterTomorrow();
				 else if (containsWord(date_elements, "tomorrow")) 
					d = DateHelper.setTomorrow();
				
				//it contains week end or weekend?
				else if (containsWord(date_elements, "weekend") 
						|| containsWords(date_elements, week_end)) {

					
					// in the case "this" week end or just "week end"
					d = DateHelper.setWeekend(true);
					if (containsWord(date_elements,"next"))
						d = DateHelper.setWeekend(false);

				} 
				
				//it contains one day of the week?
				else if (this.containsWeekDay(date_elements) != -1) {
					if (date_elements.contains("next"))
						d = DateHelper.setDayOfWeek(true, containsWeekDay(date_elements));
					if (date_elements.contains("this"))
						d = DateHelper.setDayOfWeek(false, containsWeekDay(date_elements));
					else
						d = DateHelper.setDayOfWeek(false, containsWeekDay(date_elements));

				} 
				
				//it contains the word month?
				else if (date_elements.contains("month")) {
					d = DateHelper.setNextMonth();
				}

				// TODO: in this case, it cannot be alone in the sentence. It should
				// exist a day, or other temporal
				// definition, such as "end", "beginning", etc
				else if (containsWord(date_elements, "week")) {
					if (containsWord(date_elements, "this"))
						d = DateHelper.setWeek(true);
					if (containsWord(date_elements,"next"))
						d = DateHelper.setWeek(false);

				}

				// if contains a month name?
				else if (containsMonth(date_elements)) {

					// in the format "16th of"
					for (Word date_element : date_elements) {
						matcher_exact_date = pattern_exact_date.matcher(date_element.word); //for each element of the date_elements, look for the pattern 16th
						if (matcher_exact_date.matches()) {

							matched_date = matcher_exact_date.group().split("(st|nd|rd|th)")[0];
						
						} else {
							for (String month_name : month_names) {
								if (date_element.word.equalsIgnoreCase(month_name)) {
									matched_month = date_element;
								}
							}
						}
					}

					// it can be also "the 16 of" --> look for this pattern in the sentence
					for (Sentence sentence : text.getSentences()) {
						for (Word word : sentence.getWords()) {
							bufferString.append(word.word + " ");
						}
					}

					String joinedString = bufferString.toString();
					
					// apply the regex at the sentence level
					matcher_error_date = pattern_error_date.matcher(joinedString);

					if (matcher_error_date.find()) {
						matched_date = matcher_error_date.group(1);
						System.out.println(matched_date);
					}
					

					// TODO: another form could be "July twenty four"
					if (matched_month != null) {
						Word next_word = GeneralHelper.nextWord(matched_month, text);
						if (next_word != null && next_word.ner.equals(NERTagset.NUMBER.toString())) {
							// check if there exists another number after this
							Word after_next_word = GeneralHelper.nextWord(next_word, text);

							if (after_next_word != null) {
								// two words that form a number
								String[] words_number = { next_word.word, after_next_word.word };

								bufferString = new StringBuffer(); //building the string from an array
								for (String word : words_number) {
									bufferString.append(word + " ");
								}

								System.out.println(bufferString.toString());
								matched_date = Integer.toString(NumberNormalizer.wordToNumber(bufferString.toString()).intValue());
							} else
								matched_date = NumberNormalizer.wordToNumber(next_word.word).toString();
						}
					}		
				}
					

				// all the date and the name of the month have been found in the
				// sentence
				if (matched_date != null && matched_month != null)
					d = DateHelper.setExactDate(matched_date,
							matched_month.word);
			}
		}

		return d;
	}

	// --------------------------------------------------------------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------PRIVATE SUPPORT METHODS------------------------------------------------------------
	// --------------------------------------------------------------------------------------------------------------------------------------------------
	private boolean containsMonth(List<Word> date_elements) {
		boolean monthFound = false;
		for (Word date_element : date_elements) {
			for (String month : month_names) {

				// System.out.println(date_element);
				if (date_element.word.equalsIgnoreCase(month)) {
					monthFound = true;
					break;
				} else
					monthFound = false;
			}
		}

		return monthFound;
	}

	private int containsWeekDay(List<Word> date_elements) {
		for (Word date : date_elements) {
			for (i = 0; i < weekdays.length; i++) {
				if (date.word.equalsIgnoreCase(weekdays[i])) {
					return i;
				}
			}
		}

		return -1;
	}

	private boolean containsWord(List<Word> date_elements, String word) {
		boolean wordFound = false;
		for (Word date_element : date_elements) {
			if (date_element.word.equalsIgnoreCase(word))
				wordFound = true;
			
		}

		return wordFound;

	}
	
	private boolean containsWords(List<Word> date_elements, List<String> words) {
		boolean wordsFound = false;
		int counter = 0;
		
		
		for (String word: words) {
			for (Word current_element: date_elements) {
				if (current_element.lemma.equalsIgnoreCase(word)) {
					counter ++;
				}
 			}
		}
		
		if (counter == words.size()) {
			wordsFound = true;
		}
	return wordsFound;	
	}
	
	
	
	
}
