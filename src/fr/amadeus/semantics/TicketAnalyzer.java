package fr.amadeus.semantics;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.stanford.nlp.ie.NumberNormalizer;
import fr.amadeus.helpers.GeneralHelper;
import fr.amadeus.models.Sentence;
import fr.amadeus.models.Text;
import fr.amadeus.models.Word;
import fr.amadeus.tagsets.NERTagset;
import fr.amadeus.tagsets.PennTreebankTagset;

public class TicketAnalyzer {
	
	Text text;
	List<Word> number_elements;
	int number_tickets;
	boolean isDigit = false;
	
	static Pattern pattern_digit_number;
	static Matcher matcher_digit_number;
	
	public TicketAnalyzer(Text text, HashMap<String, List<Word>> entities) {
		this.text = text;
		number_elements = entities.get(NERTagset.NUMBER.name());
		number_tickets = 1;
		
		pattern_digit_number = Pattern.compile("\\d"); //regex for digit number
		
	}
 
	
	
	
	
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//--------------------------------------------------------------NUMBER ANALYSIS------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------------		
	public int ticketAnalysis () {
		
		int number_occurrencies = number_elements != null ? number_elements.size() : 0; 

		
		//analysis on the POS tag at this point
		for (Sentence sentence: text.getSentences()) {
			for (Word word: sentence.getWords()) {
				
				if (word.pos.equals(PennTreebankTagset.PRONOUN_PERSONAL.toString())) {
					if ( !word.word.equalsIgnoreCase("we")) 
						number_tickets = 1;
					else 
						number_tickets = 2;
				}
			}
		}
		
		for (Sentence sentence: text.getSentences()) {
			for (Word word: sentence.getWords()) {
				
				//the pronoun must be present in the sentence
				if (word.lemma.equalsIgnoreCase("family")) {
					number_tickets += 2;
				}
				if (word.lemma.equalsIgnoreCase("friend") && word.pos.equals(PennTreebankTagset.NOUN.toString())) {
					number_tickets += 1;
				}
				if (word.lemma.equalsIgnoreCase("wife") 
						|| word.lemma.equalsIgnoreCase("husband") 
						|| word.lemma.equalsIgnoreCase("girlfriend") 
						|| word.lemma.equalsIgnoreCase("boyfriend")) {
					number_tickets += 1;
				}	
			}
		}
			
		
		
		if (number_occurrencies >= 1 ) {  //number more than one, check if one of them 
			
			for (Word current_number: number_elements) {
				isDigit = false; //to default the number of tickets is expressed as word
				
				//MATCHER FOR DIGIT PATTERN
				matcher_digit_number = pattern_digit_number.matcher(current_number.lemma); //matcher object
				if (matcher_digit_number.matches()) {
					isDigit = true;
				}
				
				//CATCH THE PREVIOUS AND NEXT WORD
				Word previous_word = GeneralHelper.previousWord(current_number, text);
				Word next_word = GeneralHelper.nextWord(current_number, text);
			
				
				//CHECK SOME KEYWORDS AFTER AND BEFORE
				if (next_word != null) {
					if (next_word.lemma.equalsIgnoreCase("ticket"))
						if(!isDigit)
							number_tickets = NumberNormalizer.wordToNumber(current_number.word).intValue();
						else
							number_tickets = Integer.parseInt(current_number.word);
				}
				else if (previous_word != null) {
					if (previous_word.pos.equals(PennTreebankTagset.CONJUNCTION_SUBORDINATING.toString())) {
						if(!isDigit)
							number_tickets += NumberNormalizer.wordToNumber(current_number.word).intValue();
						else
							number_tickets += Integer.parseInt(current_number.word);
					}
					else if (previous_word.lemma.equalsIgnoreCase("be")) {
						if(!isDigit)
							number_tickets = NumberNormalizer.wordToNumber(current_number.word).intValue();
						else
							number_tickets = Integer.parseInt(current_number.word);
						}
					else if (previous_word.lemma.equalsIgnoreCase("in")) {
						if(!isDigit)
							number_tickets = NumberNormalizer.wordToNumber(current_number.word).intValue();
						else
							number_tickets = Integer.parseInt(current_number.word);
					}
				}
			}	
		}
		
		
	return number_tickets;
	}
	
	
	
}
