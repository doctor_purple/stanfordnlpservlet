package fr.amadeus.semantics;

import java.util.HashMap;
import java.util.List;
import fr.amadeus.helpers.GeneralHelper;
import fr.amadeus.helpers.LocationHelper;
import fr.amadeus.models.Text;
import fr.amadeus.models.Word;
import fr.amadeus.tagsets.NERTagset;
import fr.amadeus.tagsets.PennTreebankTagset;

public class LocationAnalyzer {
	
	public LocationAnalyzer() {}
	
	//data structure
	Text text;
	List<Word> location_elements;
	List<Word> station_elements;

	//utility variables
	String destination = null;
	String origin = null;
	HashMap<String, List<String>> locations; //maintains the order of insertions
	Word previous_word, next_word, current_word;
	StringBuffer stringBuffer;
	int i=0, j=0;
	
	//static names label
	static String STATION_LABEL = NERTagset.STATION.name();
	static String LOCATION_LABEL = NERTagset.LOCATION.name();
	
	//coordinates of the device
	float latitude;
	float longitude;
	
	public LocationAnalyzer(Text text, HashMap<String, List<Word>> entities, float latitude, float longitude) {
		this.text = text;
		location_elements = entities.get(LOCATION_LABEL); //data structure with label LOCATION
		station_elements = entities.get(STATION_LABEL); //data structure with label STATION
		
		locations = new HashMap<String, List<String>>(); //Array <Name, Coordinates>
		stringBuffer = new StringBuffer(); //String buffer 
		
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	
	
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------LOCATION AND STATION ANALYSIS------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------------	
	/*
	 * Constructs the locations hasmap with ORIGIN and DESTINATION
	 * @return the hashmap object to the General Semantic Class
	 */
	public HashMap<String, List<String>> locationAnalysis () {
		//check the elements of a particular label
		//The STATION label is more selective
		checkWords(station_elements, STATION_LABEL);
		checkWords(location_elements, LOCATION_LABEL);

		//CREATE THE LOCATIONS ELEMENT, WITH NAME AND COORDINATES
		if (destination != null && origin == null) {
			System.out.println("origin missing");
			locations.put("origin", LocationHelper.getCityName(latitude, longitude));
			locations.put("destination", LocationHelper.getCoordinates(destination));
		}
		else if (origin != null && destination == null) {
			locations.put("origin", LocationHelper.getCoordinates(origin));
			
				locations.put("destination", LocationHelper.getCoordinates(destination));
		}	
		else if (origin != null && destination != null){
			locations.put("destination", LocationHelper.getCoordinates(destination));
			locations.put("origin", LocationHelper.getCoordinates(origin));
		}
		
		return locations;
	}
	
	

	
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	//---------------------------------------------------------------PRIVATE METHODS------------------------------------------------------------
	//--------------------------------------------------------------------------------------------------------------------------------------------------
	/*
	 * Checks the words after and before either for STATION label, either for LOCATION 
	 * @param elements for a particular NER label
	 */
	private void checkWords (List<Word> elements, String label) {
		//check to the label
		if (elements != null) {
			
			for (i=0; i<elements.size(); i++) {
				current_word = elements.get(i);
				previous_word = GeneralHelper.previousWord(current_word, text);
				next_word = GeneralHelper.nextWord(current_word, text);
				stringBuffer = new StringBuffer();
				
				if (previous_word != null) {
					//there is "from" before
					if (previous_word.word.equalsIgnoreCase("from")) {
						
						if ((next_word != null && !next_word.ner.equals(label)) || next_word == null) 
							origin = current_word.word;
	
						else {
							j = i;
							while (next_word != null && next_word.ner.equals(label)) {
								current_word = elements.get(j);
								next_word = GeneralHelper.nextWord(current_word, text);
								
								if (next_word == null || !next_word.ner.equals(label)) {
									stringBuffer.append(current_word.word);
								}
								else 
									stringBuffer.append(current_word.word + " ");
								
								j++;
							}
							
							origin = stringBuffer.toString();
						}	
					}
					
					
					//there is "to" before, "for" or sentence
					else if (previous_word.pos.equals(PennTreebankTagset.TO.toString())
							|| previous_word.ner.equals(label)
							|| previous_word.word.equalsIgnoreCase("for")
							|| previous_word.word != null) {
						if ((next_word != null && !next_word.ner.equals(label)) || next_word == null) 
							destination = current_word.word;
						
						else { 		//when it is multiple, the tokens are put inside a string buffer
							j = i;
							while (next_word != null && next_word.ner.equals(label)) {
								current_word = elements.get(j);
								next_word = GeneralHelper.nextWord(current_word, text);
								
								if (next_word == null || !next_word.ner.equals(label)) {
									stringBuffer.append(current_word.word);
								}
								else
									stringBuffer.append(current_word.word + " ");
								
								
								
								j++;
							}
										
							destination = stringBuffer.toString();			
						}				
					}		
				}
			}
		}		
	}
	
	
	
	/*
	 * Checks only if is present one station or location name, and takes the first one
	 */
	
	private void checkWord (List<Word> elements,String label) {
		
		
		//check to the label
		if (elements != null) {
			
			for (i=0; i<elements.size(); i++) {
				current_word = elements.get(i);
				previous_word = GeneralHelper.previousWord(current_word, text);
				next_word = GeneralHelper.nextWord(current_word, text);
				stringBuffer = new StringBuffer();
				
				if((previous_word != null && !previous_word.ner.equals(label)) || previous_word == null) {
					if ((next_word != null && !next_word.ner.equals(label)) || next_word == null) 
						destination = current_word.word;

					else {
						j = i;
						while (next_word != null && next_word.ner.equals(label)) {
							current_word = elements.get(j);
							next_word = GeneralHelper.nextWord(current_word, text);
							
							if (next_word == null || !next_word.ner.equals(label)) {
								stringBuffer.append(current_word.word);
							}
							else 
								stringBuffer.append(current_word.word + " ");
							
							j++;
						}
						
						destination = stringBuffer.toString();
					}	
				}
			}
		}
	}	
}
