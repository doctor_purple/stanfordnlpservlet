package fr.amadeus.semantics;

import java.util.HashMap;
import java.util.List;

import edu.stanford.nlp.ie.NumberNormalizer;
import fr.amadeus.helpers.GeneralHelper;
import fr.amadeus.models.Text;
import fr.amadeus.models.Word;
import fr.amadeus.tagsets.NERTagset;

public class ClassAnalyzer {
	Text text;
	List<Word> class_elements;
	
	int class_ticket = 2; //default class tickets
	
	
	
	public ClassAnalyzer(Text text, HashMap<String, List<Word>> entities) {
		this.text = text;
		class_elements = entities.get(NERTagset.CLASS.name());
	}

	public int classAnalysis() {
		if (class_elements != null) {
			int class_occurencies = class_elements.size();
			
			if (class_occurencies == 1) {
				Word current_word = class_elements.get(0);
				Word previous_word = GeneralHelper.previousWord(current_word, text);
				
				if (previous_word != null) {
					if (previous_word.ner.equals(NERTagset.ORDINAL.name())) {
						class_ticket = NumberNormalizer.wordToNumber(previous_word.lemma).intValue(); //transform the cardinal number in digit
					}
					else if (previous_word.lemma.equalsIgnoreCase("business")) {
						//class_ticket = previous_word.lemma;
						class_ticket = 1;
					}
				}
			}
		}

		return class_ticket;
	}

}
