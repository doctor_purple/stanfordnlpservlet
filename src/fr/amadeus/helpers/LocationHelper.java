package fr.amadeus.helpers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.google.code.geocoder.Geocoder;
import com.google.code.geocoder.GeocoderRequestBuilder;
import com.google.code.geocoder.model.GeocodeResponse;
import com.google.code.geocoder.model.GeocoderRequest;
import com.google.code.geocoder.model.GeocoderResult;

import fr.amadeus.models.CRBcity;

/**
 * 
 * static class to manage the location
 * @author dbattaglino
 *
 */

public class LocationHelper {
	private static String crbURL = "http://ncetsplnx46:8095/CRBConnector/crb/suggest"; 
	private static String closestCitysURL = "http://ncetsplnx46:8095/CRBConnector/crb/getClosestStation";
	
	public static List<String> getCoordinates (String cityName) {
		List<String> list = new ArrayList<String>();
		
		Geocoder geoCoder = new Geocoder(); 
		GeocoderRequest geocoderRequest = new GeocoderRequestBuilder().
												  setAddress(cityName).
												  getGeocoderRequest();
		
		GeocodeResponse geocoderResponse = geoCoder.geocode(geocoderRequest);
		if (geocoderResponse != null) {
			List<GeocoderResult> results = geocoderResponse.getResults();
			float latitude = results.get(0).getGeometry().getLocation().getLat().floatValue();
			float longitude = results.get(0).getGeometry().getLocation().getLng().floatValue();
		
			//RETRIVES THE CRB CODE
			String CRBCode = null;
			try {
				CRBCode = AmadeusHelper.getCRBCode(crbURL, cityName, latitude, longitude);
			} 
			catch (IOException e) {
				System.out.println(e.getMessage()); 
			}
			
			
			list.add(cityName);
			list.add(Float.toString(latitude));
			list.add(Float.toString(longitude));
			list.add(CRBCode);
			return list;
		}
		else 
			return null;
		
	}
	
	
	public static List<String> getCityName(float latitude, float longitude) {
		List<String> list = new ArrayList<String>();
		CRBcity closestCity = null;
		
		//try to find the nearest between all the cities in the cache
		if (closestCity == null) {
			try {
				closestCity = AmadeusHelper.getCRBCode(closestCitysURL, latitude, longitude);
			} 
			catch (IOException e) {
				System.out.println(e.getMessage());
			}
		}
		
		if (closestCity != null) {
			//how to display the results in lower case with the first capital letter
			String closestCityName = WordHelper.capitalizeFirstCharacter(closestCity.getName());
			list.add(closestCityName);
			list.add(Float.toString(closestCity.getLat()));
			list.add(Float.toString(closestCity.getLng()));
			list.add(closestCity.getId());
			return list;
		}
		return null;	
	}
}
