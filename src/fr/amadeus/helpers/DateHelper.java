package fr.amadeus.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;



/**
 * static class to manage the date of the semantic
 * @author dbattaglino
 *
 */
public class DateHelper {

	private DateHelper () {}
	
	
	public static Date setTomorrow () {
		Date d = new Date();
		//add one day to the current date and print in a standard format
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DATE, 1);
		d = c.getTime();
		
		return d;
	}
	
	public static Date setAfterTomorrow () {
		Date d = new Date();
		//add one day to the current date and print in a standard format
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DATE, 2);
		d = c.getTime();
		
		return d;
	}
	
	public static Date setWeekend (boolean thisWeekend) {
		Date d = new Date();
		Calendar c = Calendar.getInstance();
		
		if (thisWeekend)
			c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		else {
			c.add(Calendar.WEEK_OF_YEAR, 1); 
			c.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		}
			
		d = c.getTime();
		
		return d;
		
	}
	
	public static Date setDayOfWeek (boolean thisWeek, int weekDay) {
		Date d = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		
		int today_dayOfWeek = c.get(Calendar.DAY_OF_WEEK);
		int days_to_add = 0;
		
		 //get the difference in day between today and the day expressed in the natural query
		if (weekDay > today_dayOfWeek)
			c.add(Calendar.DATE, (weekDay - today_dayOfWeek));
	
		if ( weekDay < today_dayOfWeek) {
			days_to_add = Math.abs(weekDay - today_dayOfWeek);
			days_to_add = 7 - days_to_add; 

			c.add(Calendar.DATE, days_to_add);
		}
		
		
		d = c.getTime();
		
		return d;
		
	}
	
	public static Date setNextMonth () {
		Date d = new Date();
		Calendar c = Calendar.getInstance();
		c.setTime(d);

		//Add the next month
		c.add(Calendar.MONTH, 1);
		c.set(Calendar.DAY_OF_MONTH, 1);

		d = c.getTime();
		
		return d;
	}
	
	public static Date setWeek(boolean thisWeek) {
		Date d = new Date();
		Calendar c = Calendar.getInstance();
		
		if(thisWeek) {
			//TODO: how to menage this situation
			c.setTime(d);
		}
		else {
			//Add the next week
			c.add(Calendar.WEEK_OF_YEAR, 1);
			//Set day to monday
			c.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		}
		
		d = c.getTime();
		
		return d;
		
	}
	
	 public static Date setExactDate (String day_number, String month) {
		DateTimeFormatter format = DateTimeFormat.forPattern("MMM");
		DateTime instance = format.withLocale(Locale.ENGLISH).parseDateTime(month);
			
		int month_number = instance.getMonthOfYear();
			
		Date d = new Date();
		Date current = new Date();
		
		Calendar c = Calendar.getInstance();
		current = c.getTime();
		
		c.set(Calendar.MONTH, month_number - 1);
		c.set(Calendar.DATE, Integer.parseInt(day_number));
		d = c.getTime();
		
		//compare the date, so if is in the future, I select always the next
		if (d.before(current)) {
			c.add(Calendar.YEAR, 1);
			d = c.getTime();
		}	
		
		return d;
	 }
	
	
	
	
	public static String toString (Date d) {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormat.format(d);
	}
	
	public static String currentDate() {
		Date current = new Date();
		Calendar c = Calendar.getInstance();
		current = c.getTime();
		return toString(current);
	}
}
