package fr.amadeus.helpers;

public class WordHelper {
	private WordHelper() {}
	
	public static String capitalizeFirstCharacter (String sentence) {
		final StringBuilder result = new StringBuilder(sentence.length());
		sentence = sentence.toLowerCase();
		String[] words = sentence.split("\\s");
		for (int i = 0; i<words.length; i++) {
			if (i > 0) result.append(" ");
			result.append(Character.toUpperCase(words[i].charAt(0)))
				  .append(words[i].substring(1));
		}
		
		return result.toString();
	}

}
