package fr.amadeus.helpers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class SQLiteHelper {
	

	private static String DB_PATH = "/db/rai_crb_pack.db"; //local path to the db in the project
	private static String TABLE_NAME = "RAI_CRB_LOCATION_PROVIDER";
	private static String ANNOTATOR_PATH = "/src/fr/amadeus/regexner/stations_annotator";

	private SQLiteHelper() {}
	
	
	
	/*
	 * Connects to .db in sqlite, and update the file of stations, used by annotators
	 * to categorize the names of stations
	 */
	
	public static void createStationsAnnotators () throws IOException {
		Connection connection = null;
		Statement statement = null;
		File file = new File(ANNOTATOR_PATH);
		FileWriter filewriter = new FileWriter(file.getAbsoluteFile());
		BufferedWriter bufferedWriter = new BufferedWriter(filewriter);
		
		try {
			
			//load the sqlite-JDBC driver using the current class loader
			Class.forName("org.sqlite.JDBC");
			
			//create the connection with the db
			connection = DriverManager.getConnection("jdbc:sqlite:"+DB_PATH);
			statement = connection.createStatement();
			
			//SELECTION OF THE STATIONS
			ResultSet resultSet = statement.executeQuery("SELECT DISTINCT NAME FROM "+TABLE_NAME);
			while (resultSet.next()) {
				
				String current_station = resultSet.getString("NAME");
				
				//OPERATION ON THE STRING OF TEXT
				//1. lower case
				current_station = current_station.toLowerCase();
				//2. remove the brackets
				current_station = current_station.replaceAll("\\(|\\)", "");
				//3. Capital letters to the word after the space
				String[] tokens = current_station.split(" ");
				StringBuffer uppercase_station = new StringBuffer();
				
				for (String token: tokens) {
					if (!token.equals("")) {
						token = Character.toUpperCase(token.charAt(0)) + token.substring(1, token.length());
						
						if (uppercase_station.length() != 0) 
							uppercase_station.append(" " + token);
						else
							uppercase_station.append(token);
					}	
				}
				
				//4. write the uppercase_string to the annotators file
				bufferedWriter.write(uppercase_station.toString() + "\t" + "STATION" + "\t" + "O,LOCATION,PERSON,ORGANIZATION"); //the tab \t is for the custom annotator of the station 
																															   //The second tab is for the order of priority

				bufferedWriter.write(System.getProperty("line.separator"));

			}
			
			bufferedWriter.write("class" + "\t" + "CLASS");
			
			//CLOSING CONNECTIONS, STATEMENTS, RESULTSET, FILE
			bufferedWriter.close();
			resultSet.close();
			statement.close();
			connection.close();
			
		}
		catch (Exception e) {
			System.out.println("Error: " + e.getMessage());
		}
	}

}
