package fr.amadeus.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import fr.amadeus.models.CRBcity;



public class AmadeusHelper {
	private static String QUERY = "query";
	private static String LIMIT = "limit";
	private static String LIMIT_NUMBER = "10";
	private static String LAT = "lat";
	private static String LNG = "lng";
	private static String enc = "UTF-8";
	private static double earthRadius = 6371; // earth radius in km
	private static double radius = 100; // km distance from the center
	
	
	
	private AmadeusHelper () {}
	
	
public static String getCRBCode (String url_name, String city_name, float latitude, float longitude) throws IOException {
		
		
		String query = String.format(QUERY+"=%s", URLEncoder.encode(city_name, enc));
		query += "&" + String.format(LIMIT+"=%s", URLEncoder.encode(LIMIT_NUMBER, enc));
		URL url = new URL(url_name + "?" + query); 
		
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		
		if (connection.getResponseCode() != 200) {
			throw new RuntimeException("Failed: HTTP error code: " + connection.getResponseCode());
		}
		
		BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
		String output;
		CRBcity closesestCity = null;
		
		while ((output = br.readLine()) != null) {
			Gson gson = new Gson();
			JsonParser jsonParser = new JsonParser();
			JsonArray jsonArray = jsonParser.parse(output).getAsJsonArray();
			
			
			
			
			ArrayList<CRBcity> crb_cities = new ArrayList<CRBcity>(); //i put all the results in this data array
			for (JsonElement obj: jsonArray) {
				CRBcity single_data = gson.fromJson(obj, CRBcity.class);
				crb_cities.add(single_data);
			}
			
			closesestCity  = getNearestLocation(latitude, longitude, crb_cities);
			if (closesestCity != null)
				System.out.println("closest city; " + closesestCity.getId() + "  >>>>   " + closesestCity.getName());
		}
		
		connection.disconnect(); //close the connection
		if (closesestCity != null)
			return closesestCity.getId(); //should return only one id, basing on the similarity or on the coordinates
		else 
			return null;
	}
	
	
public static CRBcity getCRBCode (String url_name, float latitude, float longitude) throws IOException {
	
	String query = String.format(LAT+"=%s", URLEncoder.encode(Float.toString(latitude), enc));
	query += "&" + String.format(LNG+"=%s", URLEncoder.encode(Float.toString(longitude), enc));
	URL url = new URL(url_name + "?" + query); 
		
	HttpURLConnection connection = (HttpURLConnection) url.openConnection();
	connection.setRequestMethod("GET");
	
	if (connection.getResponseCode() != 200) {
		throw new RuntimeException("Failed: HTTP error code: " + connection.getResponseCode());
	}
	
	BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
	String output;
	CRBcity closestCity = null;
	
	while ((output = br.readLine()) != null) {
		Gson gson = new Gson();
		JsonParser jsonParser = new JsonParser();
		JsonObject jsonObject = jsonParser.parse(output).getAsJsonObject();
		closestCity = gson.fromJson(jsonObject, CRBcity.class); //gets the closest city
		System.out.println(closestCity.getName());	
	}
	
	connection.disconnect(); //close the connection
	if (closestCity != null)
		return closestCity; //should return the city object
	else 
		return null;
}
	
	

//SUPPORTING METHODS, determine the closest city given a range of coordinates
private static double rad(float x) {return x*(Math.PI/180); }
private static CRBcity getNearestLocation (float latitude, float longitude, ArrayList<CRBcity> crb_cities) {
		int earth_radius = 6371; //earth radius in km
		double distances[] = new double[crb_cities.size()];
		int closest = -1;
		
		if (crb_cities.size() > 0) {
			for (int i = 0; i < crb_cities.size(); i++) {
				double dLatitude = rad(crb_cities.get(i).getLat() - latitude);
				double dLongitude = rad(crb_cities.get(i).getLng() - longitude);
				double a = Math.sqrt(Math.sin(dLatitude/2)) 
						+ Math.sqrt(Math.cos(rad(latitude))) * Math.sqrt(Math.sin(dLongitude/2));
				double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
				double d = earth_radius * c;
				
				distances[i] = d;
				if (closest == -1 || d < distances[closest]) {
					closest = i;
				}	
			}
		}
		if (closest != -1) 
			return crb_cities.get(closest); //given a city name with the coordinates, returns the closest 
		else
			return null;
	}	
}


