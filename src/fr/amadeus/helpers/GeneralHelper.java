package fr.amadeus.helpers;

import fr.amadeus.models.Sentence;
import fr.amadeus.models.Text;
import fr.amadeus.models.Word;



/**
 * Helper for retrieving the previous and next word given a Text class object, and the current word
 * @author dbattaglino
 *
 */
public class GeneralHelper {
	
	static int i;
	static int j;

	private GeneralHelper () {}
	
	
	
	
	/*
	 * retrieves the previous and next word in the data structure
	 */
	public static Word previousWord (Word word, Text text) {
		Word previous = null;
		
		for (i = 0; i < text.getSentences().size(); i++) {
			Sentence current_sentence = text.getSentences().get(i);
			
			for ( j = 0; j < current_sentence.getWords().size(); j++) {
				Word current_word = current_sentence.getWords().get(j);
				
				if (current_word.position == word.position) {
					if (j > 0) {
						 previous = current_sentence.getWords().get(word.position - 1);
						 break;
					}
				}	
			}
		}
		
		return previous;
	}
	
	public static Word nextWord (Word word, Text text) {
		Word next = null;
		
		for (i = 0; i < text.getSentences().size(); i++) {
			Sentence current_sentence = text.getSentences().get(i);
			
			for ( j = 0; j < current_sentence.getWords().size(); j++) {
				Word current_word = current_sentence.getWords().get(j);
				if (j < current_sentence.getWords().size() - 1) {
					if (current_word.position == word.position) {
						next = current_sentence.getWords().get(word.position + 1);
						break;
					}
				}	
			}
		}
		
		return next;
		
	}
	
	
}
