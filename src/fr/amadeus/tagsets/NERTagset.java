package fr.amadeus.tagsets;

public enum NERTagset {
	/*
	 * These categories include the ner label (named entity recognizer), 
	 * in this specific implementation there are recognized LOCATION, DATE, NUMBER
	 * etc.
	 * 
	 */
	
	
	LOCATION("LOCATION"), 
	STATION("STATION"),
	CLASS("CLASS"),
	DATE("DATE"),
	NUMBER("NUMBER"),
	ORDINAL("ORDINAL"),
	MONEY("MONEY"), 
	PERSON("PERSON");
	
	private final String ner_tag;
	
	private NERTagset(String ner_tag) {
		this.ner_tag = ner_tag;
	}
	
	public String toString() {
		return getTag();
	}
	
	protected String getTag() {
		return this.ner_tag;
	}
	
	 public static NERTagset get( String value ) {
		    for( NERTagset v : values() ) {
		      if( value.equals( v.getTag() ) ) {
		        return v;
		      }
		    }

		    throw new IllegalArgumentException( "Unknown label of speech: '" + value + "'." );
		  
		  }	  

}
