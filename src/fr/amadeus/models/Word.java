package fr.amadeus.models;

import fr.amadeus.tagsets.PennTreebankTagset;

public class Word {
	
	public String word;
	public String pos;
	public String lemma;
	public String ner;
	public int position;
	


    public Word(int position, String word, String pos, String lemma, String ner){
		this.position = position;
    	this.word = word;
		this.pos = pos;
		this.lemma = lemma;
		this.ner = ner;
		
	}
    
    public String toString() {
    	return "word: "+word+" ----> "+lemma+" POS: "+PennTreebankTagset.get(pos).name()+"//// NER: "+ner + " @position: " + position;
    }
    
    
    
    
    public boolean equals(Object o) {
    	if (o instanceof Word) {
    		Word w = (Word) o;
    		return this.word.equalsIgnoreCase(w.word);
    	}
    	return false;
    }
    
    public int hashCode() {
    	return word.hashCode();
    }

}
