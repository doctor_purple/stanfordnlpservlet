package fr.amadeus.models;


/*
 * Class for automatically parsing the json from amadeus
 * 
 */
public class CRBcity {
	private String name;
	private String id;
	private float lat;
	private float lng;
	
	
	public String getName() {
		return name;
	}
	public String getId() {
		return id;
	}
	public float getLat() {
		return lat;
	}
	public float getLng() {
		return lng;
	}	
}	

