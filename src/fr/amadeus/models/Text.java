package fr.amadeus.models;

import java.util.ArrayList;


public class Text {
	ArrayList<Sentence> sentences = new ArrayList<Sentence>();
	
	public void pushSentence(Sentence sent) {
		sentences.add(sent);

	}
	
	public ArrayList<Sentence> getSentences() {
		return sentences;
	}
}
