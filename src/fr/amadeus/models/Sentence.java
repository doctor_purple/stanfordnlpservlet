package fr.amadeus.models;
import java.util.ArrayList;




public class Sentence {
	ArrayList<Word> words = new ArrayList<Word>();
	
	public void pushWord(Word word) {
		words.add(word);
	}
	
	public ArrayList<Word> getWords() {
		return words;
	}
}
